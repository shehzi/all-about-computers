## Computers - not as complicated  

Shehzaman Khatib  
Consultant, SJRI 

---

### CPU - Central Processing Unit

* Speed - 2 to 4 GHz processing speed
* Core count - physical cores / multi threading
* Intel - Core i3, i5, i7, i9 | Xeon
* AMD - Ryzen 3, 5, 7 | Epyc | Threadripper
* Socket type - LGA2011, AM4
* [Benchmark](https://www.cpubenchmark.net/)

---

### Intel

![IntelCPU](images/intel_lake.png)

---

### AMD

![AMDCPU](images/amd_ryzen.jpg)

---

### Motherboard - chooses CPU, CPU chooses motherboard

![Motherboard](images/mthbrd_form_factors.jpg)

---

### RAM - Random Access Memory

* Determines performance of computer after CPU
* CPU is fast GHz, RAM is fast too 1866 MHz 
* DDR3 and DDR4

---

### RAM 

![RAM](images/ram.jpg)

---

### Storage

* HDD - Hard Disk Drive
  * RPM - 5400, 7200, 9600
  * Size - 2.5" SATA, 3.5" SATA
  * Trusted - Wester Digital, Toshiba. (avoid Seagate)
  * Internal External Storage - can be converter
* SSD - Solid State Drive
  * Size - 2.5" SATA, M.2 SATA, M.2 mSATA, M.2 NVMe PCIe
  * Expensive - use for OS and applications
  * Buy one - Samsung, Crucial, WD

---

### HDD

![HDD](images/hdd.jpg)

---

### SSD

![SSD](images/ssd.jpg)

---

### GPU - Graphics Processing Unit

* Nvidia(GTX and RTX) vs AMD(Radeon, Radeon7, Vega) 
* Parallel computing powerhouse
* Compute Images, application in Machine Learning
* Compute Cores (RED - Compute Units)
* Frequency - 1400 MHz or higher 
* Stream Processors (Green - CUDA cores)
* GPU RAM - 8GB is high end
* GPU prices are currently over-inflated


---

### Nvidia

![Nvidia](images/nvidia.jpg)

---

### AMD - Advanced Micro Devices

![Radeon](images/amd.jpg)


---

## Other parts

---

### PSU - Power Supply Unit - efficiency and wattage

![PSU](images/psu.jpg)

---

### Case

![Case](images/case.jpg)

---

### Cooling

![Fan](images/fan.jpg)

---

### Cooling

![Liquid](images/liquid.jpg)

---

### Monitor Keyboard and Mouse

![monitor](images/monitor.jpg)

---

### Assemble a full Desktop computer

* All parts can be checked for compatibility
* [https://pcpartpicker.com](https://pcpartpicker.com)
* Example: [https://pcpartpicker.com/list/gwL4WD](https://pcpartpicker.com/list/gwL4WD)

---

### Buying a Laptop

* Price (INR 60,000 to 80,000) - shopping seasons, November/December is a good time to buy
* Weight - always go light weight
* RAM - 8GB +
* SSD - always do not buy if no SSD
* Battery life
* Graphics - trade off with weight and battery life
* Brands - Dell XPS, HP Specter, LG Gram, MacBook Air/Pro, Lenovo X1 Carbon

---

### Maintaining a computer

* Never fill your SSD leave about 20% space.
* about 10% in HDD (or 15GB which ever is smaller)
* Clean regularly - especially around fans
* Ad Blocker - [ublock origin](https://github.com/gorhill/uBlock) (not ublock) no Ad Block Plus
* No need of Anti Virus - just don't clink on phishing links

---

### Open your computer!

![desktop](images/desktop.jpg)

---

### Open Laptop - Google your specific model

![laptop](images/laptop.jpg)

---

### Helpful Resources

* https://pcpartpicker.com/
* https://www.cpubenchmark.net/
* https://www.youtube.com/user/LinusTechTips
* https://www.logicalincrements.com/
* https://www.ifixit.com/

# Thank You!